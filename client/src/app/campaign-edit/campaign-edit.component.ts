import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute, Router } from '@angular/router';
import { CampaignService } from '../shared/campaign/campaign.service';
import {FormGroup, NgForm} from '@angular/forms';

@Component({
  selector: 'app-campaign-edit',
  templateUrl: './campaign-edit.component.html',
  styleUrls: ['./campaign-edit.component.css']
})
export class CampaignEditComponent implements OnInit, OnDestroy {
  campaign: any = {};

  sub: Subscription;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private campaignService: CampaignService){}

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      const id = params['id'];
      if (id) {
        this.campaignService.get(id).subscribe((campaign: any) => {
          if (campaign) {
            this.campaign = campaign;
            this.campaign.href = campaign._links.self.href;
          } else {
            console.log(`campaign with id '${id}' not found, returning to list`);
            this.gotoList();
          }
        });
      }
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  gotoList() {
    this.router.navigate(['/campaign-list']);
  }

  save(form: NgForm) {
    this.campaignService.save(form).subscribe(result => {
      this.gotoList();
    }, error => console.error(error));
  }

  remove(href) {
    this.campaignService.remove(href).subscribe(result => {
      this.gotoList();
    }, error => console.error(error));
  }
  towns = [
    {value: 'Kraków'},
    {value: 'Katowice'},
    {value: 'Bielsko-Biała'},
    {value: 'Munich'},
    {value: 'New Delhi'},
    {value: 'Los Angeles'},
    {value: 'Agadir'}
  ];
}
