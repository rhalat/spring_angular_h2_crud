import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {CampaignService} from './shared/campaign/campaign.service';
import { CampaignListComponent } from './campaign-list/campaign-list.component';
import {
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatListModule,
  MatToolbarModule,
  MatSelectModule,
  MatCheckboxModule, MatRadioModule, MatIconModule
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { CampaignEditComponent } from './campaign-edit/campaign-edit.component';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

const appRoutes: Routes = [
  { path: '', redirectTo: '/campaign-list', pathMatch: 'full' },
  {
    path: 'campaign-list',
    component: CampaignListComponent
  },
  {
    path: 'campaign-add',
    component: CampaignEditComponent
  },
  {
    path: 'campaign-edit/:id',
    component: CampaignEditComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    CampaignListComponent,
    CampaignEditComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatListModule,
    MatToolbarModule,
    FormsModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatCheckboxModule,
    MatRadioModule,
    MatIconModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [CampaignService],
  bootstrap: [AppComponent]
})
export class AppModule { }
