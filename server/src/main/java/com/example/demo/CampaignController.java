package com.example.demo;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collection;

@RestController
class CampaignController {
    private CampaignRepository repository;

    public CampaignController(CampaignRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/campaign")
    @CrossOrigin(origins = "http://localhost:4200")
    public Collection<Campaign> campaigns() {
        return new ArrayList<>(repository.findAll());
    }
}