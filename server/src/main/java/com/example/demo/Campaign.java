package com.example.demo;

import lombok.*;

import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Entity;

@Entity
@Getter @Setter
@NoArgsConstructor
@ToString @EqualsAndHashCode
public class Campaign {
    @Id @GeneratedValue
    private Long id;
    private @NonNull String name;
    private @NonNull Integer bidAmount;
    private @NonNull Integer campFund;
    private @NonNull Boolean status;
    private @NonNull String town;
    private @NonNull Integer radius;


    public Campaign(String name, Integer bidAmount, Integer campFund, Boolean status, String town, Integer radius) {
        this.name = name;
        this.bidAmount = bidAmount;
        this.campFund = campFund;
        this.status = status;
        this.town = town;
        this.radius = radius;
    }
}