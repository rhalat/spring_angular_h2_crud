package com.example.demo;

import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Bean
    ApplicationRunner init(CampaignRepository repository) {
        return args -> {
            repository.save(new Campaign("Kampania1", 20, 200, true, "Kraków", 200));
            repository.save(new Campaign("Kampania2", 20, 400, false, "New Delhi", 30));
            repository.save(new Campaign("Kampania3", 20, 600, true, "Katowice", 70));
            repository.findAll().forEach(System.out::println);
        };
    }
}
